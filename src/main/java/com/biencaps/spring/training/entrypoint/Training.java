package com.biencaps.spring.training.entrypoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Training {

	public static void main(String[] args) {
		SpringApplication.run(Training.class, args);
	}

}
