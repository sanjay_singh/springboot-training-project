package com.biencaps.spring.training.entrypoint;

public interface Coach {

	public String getDailyWorkout();
	public String getDailyFortune();
	
}
